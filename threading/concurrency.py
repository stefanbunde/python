from threading import Lock, Thread


a = 0

lock = Lock()


def increment():
    global a
    for i in range(1000000):
        lock.acquire()
        a += 1
        lock.release()


def decrement():
    global a
    for i in range(1000000):
        lock.acquire()
        a -= 1
        lock.release()


inc = Thread(target=increment)
dec = Thread(target=decrement)

inc.start()
dec.start()

inc.join()
dec.join()

print(a)
