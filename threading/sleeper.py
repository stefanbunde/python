import time
from threading import Thread


def sleeper(i):
    print("thread %d sleeps for 4 seconds" % i)
    time.sleep(4)
    print("thread %d woke up" % i)


threads = []

for i in range(10):
    t = Thread(target=sleeper, args=(i,))
    threads.append(t)
    t.start()

for thread in threads:
    thread.join()

print("quit main")
