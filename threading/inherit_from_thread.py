import threading
import time


class Timer(threading.Thread):
    def __init__(self, interval, thread_number):
        threading.Thread.__init__(self)
        self.interval = interval
        self.thread_number = thread_number

    def run(self):
        time.sleep(self.interval)
        print("Thread number: {}".format(self.thread_number))


if __name__ == "__main__":
    print("start main")

    p1 = Timer(3, 1)
    p2 = Timer(1.5, 2)

    p1.start()
    p2.start()

    p1.join()
    p2.join()

    print("quit main")
