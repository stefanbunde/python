from django.contrib import admin
from django.contrib.auth.views import logout
from django.urls import path

from django_otp.views import login as otp_login

from .views import index


urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', otp_login),
    path('logout/', logout),
    path('', index),
]
