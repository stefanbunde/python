from django.http import HttpResponse


def index(request):
    is_authenticated = request.user.is_authenticated
    output = ["logged in" if is_authenticated else "logged out"]
    output.append("<a href=\"/{0}\">{0}</a>".format("logout" if is_authenticated else "login"))
    return HttpResponse("<br>".join(output))
