from django.db import models


class Repository(models.Model):
    created_on = models.DateTimeField()
    description = models.TextField()
    full_name = models.CharField(max_length=128)
    is_private = models.BooleanField()
    language = models.CharField(max_length=64)
    name = models.CharField(max_length=128)

    class APIMeta:
        resource_path = "repositories/stefanbunde"
        resource_name = "values"
        resource_name_plural = "values"
