from django.http import HttpResponse
from django.template import Context, Template

from .models import Repository


html = """
<table border="1">
    <tr>
        <td>name</td>
        <td>language</td>
        <td>created on</td>
    <tr>

    {% for repo in repos %}
    <tr>
        <td>{{ repo.name }}</td>
        <td>{{ repo.language }}</td>
        <td>{{ repo.created_on }}</td>
    <tr>
    {% endfor %}
</table>
"""


def index(request):
    repos = Repository.objects.all()
    template = Template(html)
    context = Context({"repos":repos})
    return HttpResponse(template.render(context))
