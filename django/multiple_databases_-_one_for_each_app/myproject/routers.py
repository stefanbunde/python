APPS = ["app1", "app2"]


class GenericRouter(object):

    def db_for_read(self, model, **hints):
        if model._meta.app_label in APPS:
            return model._meta.app_label
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label in APPS:
            return model._meta.app_label
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label in APPS:
            if obj1._meta.app_label == obj2._meta.app_label:
                return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if db in APPS:
            return app_label == db
        if app_label in APPS:
            return False
        return None
