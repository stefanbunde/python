from django.db import models
from django.contrib.postgres.fields import JSONField


class VehicleType(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class Vehicle(models.Model):
    type = models.ForeignKey(VehicleType, on_delete=models.CASCADE)
    max_speed = models.PositiveSmallIntegerField()
    extra_data = JSONField()

    def __str__(self):
        return "{} - {}".format(self.type, self.max_speed)
