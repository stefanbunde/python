from django.db import models


class Person(models.Model):
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    birthday = models.DateField(blank=True, null=True)

    class Meta:
        permissions = (
            ("Person.first_name.read", "Can read persons first name"),
            ("Person.first_name.write", "Can update persons first name"),
            ("Person.last_name.read", "Can read persons last name"),
            ("Person.last_name.write", "Can update persons last name"),
            ("Person.birthday.read", "Can read persons birthday"),
            ("Person.birthday.write", "Can update persons birthday"),
        )
