from rest_framework import serializers

from .models import Person


class PersonSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Person
        fields = "id", "first_name", "last_name", "birthday"
