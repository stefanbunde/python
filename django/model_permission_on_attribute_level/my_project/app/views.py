from rest_framework import authentication, permissions, viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.mixins import UpdateModelMixin
from rest_framework.response import Response

from .models import Person
from .serializers import PersonSerializer


class DefaultMixin(object):
    authentication_classes = (
        authentication.BasicAuthentication,
        authentication.TokenAuthentication,
    )
    permission_classes = (
        permissions.IsAuthenticated,
    )


def has_permission(user, model_name, attr_name, mode):
    return user.has_perm("{}.{}.{}.{}".format(__package__, model_name, attr_name, mode))


def has_read_permission(user, model_name, attr_name):
    return has_permission(user, model_name, attr_name, "read")


def has_write_permission(user, model_name, attr_name):
    return has_permission(user, model_name, attr_name, "write")


class UpdateMixin(UpdateModelMixin):

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        model_name = instance.__class__.__name__
        permitted = list(filter(lambda item: not has_write_permission(request.user, model_name, item),
                                request.data))
        if permitted:
            error_msg = "Permission denied for fields: {}".format(", ".join(permitted))
            raise PermissionDenied(detail=error_msg)

        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class AttributePermissionMixin(UpdateMixin):

    def list(self, request):
        serializer = self.get_serializer(self.filter_queryset(self.get_queryset()), many=True)

        model_name = self.queryset.model.__name__
        data = [dict(filter(lambda item: has_read_permission(request.user, model_name, item[0]),
                            entry.items()))
                for entry in serializer.data]
        return Response(data)

    def retrieve(self, request, pk=None):
        instance = self.get_object()
        serializer = self.get_serializer(instance)

        model_name = instance.__class__.__name__
        data = dict(filter(lambda item: has_read_permission(request.user, model_name, item[0]),
                           serializer.data.items()))
        return Response(data)


class PersonViewSet(DefaultMixin, AttributePermissionMixin, viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer
