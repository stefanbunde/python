import os

from django.conf import settings


DEBUG = os.environ.get("DEBUG", "on") == "on"

SECRET_KEY = os.environ.get("SECRET_KEY", os.urandom(32))

ALLOWED_HOSTS = os.environ.get("ALLOWED_HOSTS", "localhost").split(",")

settings.configure(
    DEBUG=DEBUG,
    SECRET_KEY=SECRET_KEY,
    ALLOWED_HOSTS=ALLOWED_HOSTS,
    ROOT_URLCONF=__name__,
)


import sys

from django.http import HttpResponse
from django.urls import path


def index(request):
    return HttpResponse("<h1>Hello World</h1>")


urlpatterns = (
    path('', index),
)


if __name__ == "__main__":
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
