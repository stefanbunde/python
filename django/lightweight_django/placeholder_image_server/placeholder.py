import os

from django.conf import settings


DEBUG = os.environ.get("DEBUG", "on") == "on"

SECRET_KEY = os.environ.get("SECRET_KEY", os.urandom(32))

ALLOWED_HOSTS = os.environ.get("ALLOWED_HOSTS", "localhost").split(",")

BASE_DIR = os.path.dirname(__file__)


settings.configure(
    DEBUG=DEBUG,
    SECRET_KEY=SECRET_KEY,
    ALLOWED_HOSTS=ALLOWED_HOSTS,
    ROOT_URLCONF=__name__,
    INSTALLED_APPS=(
        "django.contrib.staticfiles",
    ),
    TEMPLATES=(
        {
            "BACKEND": "django.template.backends.django.DjangoTemplates",
            "DIRS": (os.path.join(BASE_DIR, 'templates'),),
        },
    ),
    STATICFILES_DIRS=(
        os.path.join(BASE_DIR, "static"),
    ),
    STATIC_URL="/static/",
)


import hashlib
import sys

from io import BytesIO
from PIL import Image, ImageDraw

from django import forms
from django.core.cache import cache
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render
from django.views.decorators.http import etag
from django.urls import path, re_path, reverse


class ImageForm(forms.Form):
    height = forms.IntegerField(min_value=1, max_value=2000)
    width = forms.IntegerField(min_value=1, max_value=2000)

    def generate(self, image_format="PNG"):
        image_width = self.cleaned_data["width"]
        image_height = self.cleaned_data["height"]

        key = f'{image_width}.{image_height}.{image_format}'
        content = cache.get(key)
        if content is None:
            image = Image.new("RGB", (image_width, image_height))

            draw = ImageDraw.Draw(image)
            text = f'{image_width} x {image_height}'
            text_width, text_height = draw.textsize(text)
            if text_width < image_width and text_height < image_height:
                text_left = (image_width - text_width) // 2
                text_top = (image_height - text_height) // 2
                draw.text((text_left, text_top), text, fill=(255, 255, 255))

            content = BytesIO()
            image.save(content, image_format)
            content.seek(0)
            cache.set(key, content, 60*60)
        return content


def generate_etag(request, width, height):
    content = f'Placeholder: {width} x {height}'
    return hashlib.sha1(content.encode("utf-8")).hexdigest()


@etag(generate_etag)
def placeholder(request, width, height):
    form = ImageForm({"width": width, "height": height})
    if form.is_valid():
        image = form.generate()
        return HttpResponse(image, content_type="image/png")
    else:
        return HttpResponseBadRequest("Invalid Image Request")


def index(request):
    example_uri = reverse("placeholder", kwargs={"width": 50, "height": 50})
    context = {
        "example": request.build_absolute_uri(example_uri),
    }
    return render(request, "index.html", context)


urlpatterns = (
    path('', index, name='index'),
    re_path('image/(?P<width>[0-9]+)x(?P<height>[0-9]+)/', placeholder, name='placeholder'),
)


if __name__ == "__main__":
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
