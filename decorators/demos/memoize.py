import time

from decorators import memoize


@memoize()
def complex_function_with_default_caching_time(a, b):
    print(" -> complex_function_with_default_caching_time({}, {})".format(a, b))
    return a + b


@memoize(1)
def complex_function_with_one_second_caching_time(a, b):
    print(" -> complex_function_with_one_second_caching_time({}, {})".format(a, b))
    return a * b


if __name__ == "__main__":
    print(50 * "*")
    print()

    print("get result from function")
    print(" -> {}".format(complex_function_with_one_second_caching_time(2, 2)))
    print()
    print("get result from cache")
    print(" -> {}".format(complex_function_with_one_second_caching_time(2, 2)))
    print()
    print("sleep for 1 second")
    time.sleep(1)
    print()
    print("get result from function")
    print(" -> {}".format(complex_function_with_one_second_caching_time(2, 2)))

    print()
    print(50 * "*")
    print()

    print("get result from function")
    print(" -> {}".format(complex_function_with_default_caching_time(2, 2)))
    print()
    print("get result from cache")
    print(" -> {}".format(complex_function_with_default_caching_time(2, 2)))
    print()
    print("sleep for 9 seconds")
    time.sleep(9)
    print()
    print("get result from cache")
    print(" -> {}".format(complex_function_with_default_caching_time(2, 2)))
    print()
    print("sleep for 1 second")
    time.sleep(1)
    print()
    print("get result from function")
    print(" -> {}".format(complex_function_with_default_caching_time(2, 2)))

    print()
    print(50 * "*")
