from django.contrib import admin

from meraxes.finance import models


class AccountAdmin(admin.ModelAdmin):
    def balance(self):
        queryset = self.transaction_set.values_list('amount', flat=True)
        balance_dm = sum(queryset.filter(currency=models.Transaction.CURRENCY.get_value('dm')))
        balance_euro = sum(queryset.filter(currency=models.Transaction.CURRENCY.get_value('euro')))
        balance = balance_dm / 1.95583 + balance_euro
        return f'{balance:.2f}'

    list_display = 'name', 'institute', balance


class TransactionAdmin(admin.ModelAdmin):
    def debit(self):
        return '' if self.amount > 0 else f'{self.amount:.2f}'

    def credit(self):
        return f'{self.amount:.2f}' if self.amount > 0 else ''

    list_display = 'date', 'account', debit, credit, 'purpose'
    search_fields = 'purpose',


admin.site.register(models.Account, AccountAdmin)
admin.site.register(models.Institute)
admin.site.register(models.Transaction, TransactionAdmin)
