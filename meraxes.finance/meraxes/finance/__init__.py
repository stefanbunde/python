# major.minor.patch.release.number
# release must be one of alpha, beta, rc or final
VERSION = (0, 2, 1, 'alpha', 1)

__version__ = '.'.join(str(x) for x in VERSION)
