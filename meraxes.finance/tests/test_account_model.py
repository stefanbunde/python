from meraxes.finance.models import Account, Institute


def test_str_method():
    institute = Institute(name='institute_name')
    account = Account(name='giro', institute=institute)

    assert str(account) == 'giro (institute_name)'
