from meraxes.finance.models import Institute


def test_str_method():
    institute = Institute(name='institute_name')

    assert str(institute) == 'institute_name'
