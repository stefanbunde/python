from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK, HTTP_403_FORBIDDEN


def test_admin_user_can_view_institutes(admin_client):
    response = list_institutes(admin_client)

    assert_success(response)


def test_user_without_permission_can_view_institutes(client):
    response = list_institutes(client)

    assert_success(response)


def test_anonymous_user_cannot_view_institutes(anonymous_client):
    response = list_institutes(anonymous_client)

    assert_permission_denied(response)


def list_institutes(client):
    return client.get(reverse('institute-list'))


def assert_success(response):
    assert response.status_code == HTTP_200_OK


def assert_permission_denied(response):
    assert response.status_code == HTTP_403_FORBIDDEN
