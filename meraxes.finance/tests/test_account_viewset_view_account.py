from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK, HTTP_403_FORBIDDEN


def test_admin_user_can_view_accounts(admin_client):
    response = list_accounts(admin_client)

    assert_success(response)


def test_user_without_permission_can_view_accounts(client):
    response = list_accounts(client)

    assert_success(response)


def test_anonymous_user_cannot_view_accounts(anonymous_client):
    response = list_accounts(anonymous_client)

    assert_permission_denied(response)


def list_accounts(client):
    return client.get(reverse('account-list'))


def assert_success(response):
    assert response.status_code == HTTP_200_OK


def assert_permission_denied(response):
    assert response.status_code == HTTP_403_FORBIDDEN
