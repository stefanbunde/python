import os
import re
import time

from urllib.parse import urljoin

from selenium import webdriver
from selenium.webdriver.chrome.options import Options


DOWNLOAD_PATH = os.path.join(os.path.expanduser("~"), "Downloads/packtpub")
ROOT_URL = "https://www.packtpub.com/"

DOWNLOADED_BOOKS_FILE = "downloads.txt"


def get_already_downloaded_books():
    if not os.path.exists(DOWNLOADED_BOOKS_FILE):
        return []

    with open(DOWNLOADED_BOOKS_FILE, "r") as fh:
        return fh.read().splitlines()


DOWNLOADED_BOOKS = get_already_downloaded_books()


def main():
    if not os.path.isdir(DOWNLOAD_PATH):
        os.mkdir(DOWNLOAD_PATH)

    options = Options()
    options.add_experimental_option("prefs", {
        "download.default_directory": DOWNLOAD_PATH,
        "download.prompt_for_download": False,
    })
    driver = webdriver.Chrome(chrome_options=options)
    driver.get(ROOT_URL)

    login(driver)

    driver.get(urljoin(ROOT_URL, "account/my-ebooks"))

    books = driver.find_elements_by_class_name("product-line")
    for index, book in enumerate(books[:-1]):
        title = get_title(book)

        if title in DOWNLOADED_BOOKS:
            continue
        print(title)

        book_directory = os.path.join(DOWNLOAD_PATH, title)
        os.mkdir(book_directory)

        download_urls = get_download_urls(book)
        for file_extension, download_url in download_urls.items():
            driver.get(download_url)
            downloaded_file = wait_for_download_complete(file_extension)
            os.rename(downloaded_file, os.path.join(book_directory, title + file_extension))

        category = get_category(book)
        move_book_directory_into_category_directory(category, book_directory, title)

        register_book_as_downloaded(title)


def login(driver):
    driver.maximize_window()
    driver.find_element_by_link_text("Log in").click()
    login_form = driver.find_element_by_id("packt-user-login-form")
    login_form.find_element_by_id("email").send_keys(os.environ.get("PACKTPUB_USERNAME"))
    login_form.find_element_by_id("password").send_keys(os.environ.get("PACKTPUB_PASSWORD"))
    login_form.find_element_by_id("edit-submit-1").click()


def get_title(book):
    title_element = book.find_element_by_class_name("title").text
    match = re.search("(.+?).?(?:\[eBook\])+", title_element)
    title = match.group(1)
    if title.startswith("."):
        title = "Dot" + title[1:]
    return title


def get_download_urls(book):
    download_container = book.find_elements_by_class_name("download-container")[1]
    urls = [url.get_property("href") for url in download_container.find_elements_by_tag_name("a")]
    download_urls = {}
    for url in urls:
        if "pdf" in url:
            download_urls[".pdf"] = url
        elif "epub" in url:
            download_urls[".epub"] = url
        elif "code_download" in url:
            download_urls[".zip"] = url
    return download_urls


def wait_for_download_complete(file_extension):
    time.sleep(0.2)
    downloaded_file = get_latest_downloaded_file()
    while not downloaded_file.endswith(file_extension):
        time.sleep(1)
        downloaded_file = get_latest_downloaded_file()
    return downloaded_file


def get_latest_downloaded_file():
    directory_content = [os.path.join(DOWNLOAD_PATH, item) for item in os.listdir(DOWNLOAD_PATH)]
    return next(item for item in directory_content if os.path.isfile(item))


def move_book_directory_into_category_directory(category, book_directory, title):
    category_directory = os.path.join(DOWNLOAD_PATH, category)
    if not os.path.exists(category_directory):
        os.mkdir(category_directory)
    os.rename(book_directory, os.path.join(category_directory, title))


def get_category(book):
    url = get_url(book)
    return url.split("/")[-2]

def get_url(book):
    thumbnail = book.find_element_by_class_name("product-thumbnail")
    return thumbnail.find_element_by_tag_name("a").get_property("href")


def register_book_as_downloaded(title):
    with open("downloads.txt", "a") as fh:
        fh.write(title + "\n")


if __name__ == "__main__":
    main()
