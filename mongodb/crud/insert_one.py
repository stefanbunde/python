from random import randint

from pymongo import MongoClient


client = MongoClient(port=27017)
db = client.business

movies = ["movie{}".format(x) for x in range(1, 21)]

for movie in movies:
    data = {
        "name": movie,
        "rating": randint(1, 5),
    }

    result = db.movie_ratings.insert_one(data)
    print("inserted {} with id {}".format(movie, result.inserted_id))

print("finished")
