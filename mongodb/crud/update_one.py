from datetime import datetime
from pprint import pprint

from pymongo import MongoClient


client = MongoClient(port=27017)
db = client.business

movie_before = db.movie_ratings.find_one({})
print("document before")
pprint(movie_before)


result = db.movie_ratings.update_one(
    {"_id": movie_before.get("_id")},
    {"$set": {"last_update": datetime.now()}})
print("number of documents modified: {}".format(result.modified_count))


movie_after = db.movie_ratings.find_one({"_id": movie_before.get("_id")})
print("document after")
pprint(movie_after)


print("finished")
