from pprint import pprint

from pymongo import MongoClient


client = MongoClient(port=27017)
db = client.business

results = db.movie_ratings.find({"name": "movie1"})

print("found {} entries:".format(results.count()))
for result in results:
    pprint(result)

print("finished")
