from pprint import pprint

from pymongo import MongoClient


client = MongoClient(port=27017)
db = client.business

print("The sum of each rating occurance across all data grouped by rating")
results = db.movie_ratings.aggregate([
    {
        "$group": {
            "_id": "$rating",
            "count": {"$sum": 1},
        },
    },
    {
        "$sort": {"_id": 1},
    },
])

for result in results:
    print(result)

print("finished")
