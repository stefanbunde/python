from random import randint

from pymongo import MongoClient


client = MongoClient(port=27017)
db = client.business

movies = ["movie{}".format(x) for x in range(1, 21)]

data = [{"name": movie, "rating": randint(1, 5)} for movie in movies]

result = db.movie_ratings.insert_many(data)
print("inserted {} movies with ids {}".format(len(data), result.inserted_ids))

print("finished")
