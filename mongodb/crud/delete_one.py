from pprint import pprint

from pymongo import MongoClient


client = MongoClient(port=27017)
db = client.business

query = {"name": "movie1"}

result = db.movie_ratings.find(query)
print("{} items before delete".format(result.count()))

db.movie_ratings.delete_one(query)

result = db.movie_ratings.find(query)
print("{} items after delete".format(result.count()))


print("finished")
